// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import PureVueChart from 'pure-vue-chart';


import 'vuetify/dist/vuetify.min.css'


import menu from '@/components/menu';
Vue.component("menu-app", menu);
import slider from '@/components/slider';
Vue.component("slider-app", slider);
import footer from '@/components/footer';
Vue.component("footer-app", footer);
import empleado from '@/components/empleado';
Vue.component("empleado-app", empleado);
 import usuario from '@/components/usuario';
 Vue.component("usuario-app", usuario);
 import admin from '@/components/admin';
 Vue.component("admin-app", admin);

Vue.use(Vuetify)



Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
