import vue from 'vue'
import axios from 'axios'
import vueAxios from 'vue-axios'

vue.use(vueAxios, axios)

import vSelect from 'vue-select';
vue.component( 'v-select', vSelect );

export default {
  data () {
    return {
      LOCAL: 0,
      REMOTE: 1,
      SERVER_ROUTE: 'http://seminario.aprendiendosobremicomportamiento.club/',
      load_type: 0
    }
  },
  methods: {
    getPng (path, callback) {
      require.context('../../assets/', true, /\.png$/)
      if (this.load_type === this.LOCAL) {
        callback(require('../../assets/' + path + '.png'))
        return
      }

      var dataUrl = this.SERVER_ROUTE + path + '.png'

      axios
        .get(dataUrl)
        .then(function (response) {
          callback(response.data)
        })
        .catch(function (error) {
          console.error(error + 'para la url ' + dataUrl)
        })
    },
    show () {
      console.log('Show')
    }
  }
}
