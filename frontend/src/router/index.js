import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import main from '@/pages/main.vue'
import cartelera from '@/pages/cartelera.vue'
import login from '@/pages/login.vue'
import evaluacion from '@/pages/evaluacion.vue'
import snacks from '@/pages/snacks.vue'
import mainAdmin from '@/pages/mainAdmin.vue'
import mainEmpleado from '@/pages/mainEmpleado.vue'
import gestpelic from '@/pages/gestpelic.vue'
import gestsnacks from '@/pages/gestsnacks.vue'
import gestestad from '@/pages/gestestad.vue'
import gestmultiplex from '@/pages/gestmultiplex.vue'
import vendertiquete from '@/pages/vendertiquete.vue'
import vendercomida from '@/pages/vendercomida.vue'

import crearcliente from '@/pages/crearcliente.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/agradecimientos',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/',
      name: 'main',
      component: main
    },
    {
      path: '/cartelera',
      name: 'cartelera',
      component: cartelera
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/evaluacion',
      name: 'evaluacion',
      component: evaluacion
    },
    {
      path: '/snacks',
      name: 'snacks',
      component: snacks
    },
    {
      path: '/mainAdmin',
      name: 'mainAdmin',
      component: mainAdmin
    },
    {
      path: '/mainEmpleado',
      name: 'mainEmpleado',
      component: mainEmpleado
    },
    {
      path: '/gestpelic',
      name: 'gestpelic',
      component: gestpelic
    },
    {
      path: '/gestsnacks',
      name: 'gestsnacks',
      component: gestsnacks
    },
    {
      path: '/gestestad',
      name: 'gestestad',
      component: gestestad
    },
    {
      path: '/gestmultiplex',
      name: 'gestmultiplex',
      component: gestmultiplex
    },
    {
      path: '/vendertiquete',
      name: 'vendertiquete',
      component: vendertiquete
    },
    {
      path: '/vendercomida',
      name: 'vendercomida',
      component: vendercomida
    },
    {
      path: '/crearcliente',
      name: 'crearcliente',
      component: crearcliente
    }
  ]
})
