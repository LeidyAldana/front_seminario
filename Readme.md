# Cine Distrito 

Front para el proyecto de Seminario de Ingeniería

Estudiantes:

Leidy Marcela Aldana ~ 20151020019
Hayder Armando Calderón ~ 
Edwin Aaron Garcia

Universidad Distrital Francisco José de Caldas
Pregrado en Ingeniería de Sistemas

## Ejecutar el proyecto:

cd frontend/ 
npm run dev

## Enlace público:

http://seminario.aprendiendosobremicomportamiento.club/#/
